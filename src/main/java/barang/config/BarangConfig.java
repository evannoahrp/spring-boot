package barang.config;

import barang.entity.Barang;
import barang.repository.BarangRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class BarangConfig {
    @Bean
    CommandLineRunner commandLineRunner(
            BarangRepository repository) {
        return args -> {
            Barang kopi = new Barang(
                    1L,
                    "Kopi",
                    10,
                    "Kg",
                    50000
            );
            Barang gula = new Barang(
                    2L,
                    "Gula",
                    5,
                    "Kg",
                    12000
            );
            repository.saveAll(
                    Arrays.asList(kopi, gula)
            );
        };
    }
}
