package barang.controller;

import barang.entity.Barang;
import barang.model.ModelBarang;
import barang.repository.BarangRepository;
import barang.service.BarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "v1/barang")
public class BarangController {

    private final BarangService barangService;
    private final BarangRepository barangRepository;

    @Autowired
    public BarangController(BarangService barangService, BarangRepository barangRepository) {
        this.barangService = barangService;
        this.barangRepository = barangRepository;
    }

    @GetMapping
    public Map getAllBarang() {
        return barangService.getAll();
    }

    @GetMapping(path = "{barangId}")
    public Map getOneBarang(@PathVariable("barangId") Long barangId) {
        return barangService.getOne(barangId);
    }

    @PostMapping
    public Map postBarang(@RequestBody Barang barang) {
        return barangService.insert(barang);
    }

    @PutMapping
    public Map updateBarang(@RequestBody Barang barang) {
        return barangService.update(barang);
    }

    @DeleteMapping(path = "{barangId}")
    public Map deleteBarang(@PathVariable("barangId") Long barangId) {
        return barangService.delete(barangId);
    }

    @GetMapping("/listpagenative")
    public Map listpagenative() {
        return barangService.getAllNative();
    }

    @GetMapping("/listpageentitas")
    public Map listpageentitas() {
        Map map = new HashMap();
        List<ModelBarang> obj = barangRepository.modelDTO();
        map.put("data", obj);
        return map;
    }
}
