package barang.controller.unittest;

import barang.model.ModelBarang;
import barang.repository.BarangRepository;
import barang.service.BarangService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class BarangControllerTest extends UnitTest {
    private final BarangService barangService;
    private final BarangRepository barangRepository;

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Autowired
    public BarangControllerTest(BarangService barangService, BarangRepository barangRepository) {
        this.barangService = barangService;
        this.barangRepository = barangRepository;
    }

    @Test
    public void dtoModel() throws Exception {
        List<ModelBarang> count = barangRepository.modelDTO();
        System.out.println("total = " + count);
        for (ModelBarang a : count) {
            System.out.println("nama = " + a.getNama());
        }
    }
}
