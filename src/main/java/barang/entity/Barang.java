package barang.entity;

import lombok.Setter;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Setter
@Getter
@Entity
@Table(name = "barang")
public class Barang extends AbstractDate implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "barang_sequence"
    )
    private Long id;

    @Column(name = "nama", nullable = false, length = 45)
    private String nama;

    @Column(name = "stok", nullable = false, length = 11)
    private Integer stok;

    @Column(name = "satuan", nullable = false, length = 45)
    private String satuan;

    @Column(name = "harga", nullable = false, length = 11)
    private Integer harga;

    @ManyToOne(targetEntity = Supplier.class, cascade = CascadeType.ALL)
    private Supplier supplier;

    @OneToMany(mappedBy = "barang")
    Set<Transaction> transactions;

    public Barang() {
    }

    public Barang(String name, Integer stok, String satuan, Integer harga) {
        this.nama = name;
        this.stok = stok;
        this.satuan = satuan;
        this.harga = harga;
    }

    public Barang(Long id, String nama, Integer stok, String satuan, Integer harga) {
        this.id = id;
        this.nama = nama;
        this.stok = stok;
        this.satuan = satuan;
        this.harga = harga;
    }

    @Override
    public String toString() {
        return "Barang{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", stok=" + stok +
                ", satuan='" + satuan + '\'' +
                ", harga=" + harga +
                '}';
    }
}
