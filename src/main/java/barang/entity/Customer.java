package barang.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "customer")
public class Customer implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "customer_sequence"
    )
    private Long id;

    @Column(name = "nama", nullable = false, length = 45)
    private String nama;

    @Column(name = "hp", length = 15)
    private String hp;

    @Column(name = "jk", length = 15)
    private String jk;

    @Column(name = "alamat", columnDefinition = "TEXT")
    private String alamat;

    @OneToOne(mappedBy = "customer")
    private CustomerDetail customerdetail;

    @OneToMany(mappedBy = "customer")
    Set<Transaction> transactions;

}
