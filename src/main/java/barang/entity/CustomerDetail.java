package barang.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "customerdetail")
public class CustomerDetail implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "customerdetail_sequence"
    )
    private Long id;

    @Column(name = "status", nullable = false, length = 45)
    private String status;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "customer_id")
    private Customer customer;

}
