package barang.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "transaction")
public class Transaction extends AuditDate implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "transaction_sequence"
    )
    private Long id;

    @ManyToOne
    @JoinColumn(name = "barang_id")
    private Barang barang;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

}
