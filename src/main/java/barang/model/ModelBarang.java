package barang.model;

import lombok.Data;

@Data
public class ModelBarang {
    private Long id;
    private String nama;
    private int stok;
    private String satuan;
    private int harga;

    public ModelBarang(Long id, String nama) {
        System.out.println("id = " + id + ", nama = " + nama);
        this.id = id;
        this.nama = nama;
    }
}
