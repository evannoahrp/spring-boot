package barang.repository;

import barang.entity.Barang;
import barang.model.ModelBarang;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarangRepository
        extends PagingAndSortingRepository<Barang, Long> {

    @Query("select b from Barang b")
    public List<Barang> getList();

    @Query("select b from Barang b WHERE b.id = :id")
    public Barang getbyID(@Param("id") Long id);

    @Query(value = "select new barang.model.ModelBarang(b.id, b.nama) from Barang b")
    List<ModelBarang> modelDTO();

    @Query(value = "SELECT b.id, b.nama FROM barang b", nativeQuery = true)
    List<Object[]> getDataAllNative();
}
