package barang.service;

import barang.entity.Barang;

import java.util.Map;

public interface BarangService {

    public Map getAll();

    public Map getOne(Long barangId);

    public Map insert(Barang barang);

    public Map update(Barang barang);

    public Map delete(Long barangId);

    public Map getAllNative();
}
