package barang.service.implement;

import barang.entity.Barang;
import barang.model.ModelBarang;
import barang.repository.BarangRepository;
import barang.service.BarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.*;

@Service
@Transactional
public class BarangImplement implements BarangService {

    @Autowired
    public BarangRepository barangRepository;

    public Map getAll() {
        List<Barang> list = new ArrayList<Barang>();
        Map<String, Object> map = new HashMap<>();
        try {
            list = barangRepository.getList();
            map.put("data", list);
            map.put("status", "200");
            map.put("message", "Get all success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error get all barang");
            return map;
        }
    }

    public Map getOne(Long barangId) {
        Map<String, Object> map = new HashMap<>();
        try {
            Barang obj = barangRepository.getbyID(barangId);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Get success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error get barang");
            return map;
        }
    }

    public Map insert(Barang barang) {
        Map<String, java.io.Serializable> map = new HashMap<>();
        try {
            Barang obj = barangRepository.save(barang);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Insert success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error insert barang");
            return map;
        }
    }

    public Map update(Barang barang) {
        Map<String, java.io.Serializable> map = new HashMap<String, java.io.Serializable>();
        try {
            Barang obj = barangRepository.getbyID(barang.getId());
            if (barang.getNama() != null &&
                    barang.getNama().length() > 0 &&
                    !Objects.equals(obj.getNama(), barang.getNama())) {
                obj.setNama(barang.getNama());
            }
            obj.setHarga(obj.getHarga());
            obj.setSatuan(obj.getSatuan());
            obj.setStok(obj.getStok());
            barangRepository.save(obj);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Update success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("message", e);
            return map;
        }
    }

    public Map delete(Long barangId) {
        Map<String, java.io.Serializable> map = new HashMap<String, java.io.Serializable>();
        try {
            boolean temp = barangRepository.existsById(barangId);
            if (temp) {
                barangRepository.deleteById(barangId);
                map.put("data", "1");
                map.put("status", "200");
                map.put("message", "Delete success");
            } else {
                map.put("data", "0");
                map.put("status", "404");
                map.put("error", "Internal Server Error");
                map.put("message", "Barang with id " + barangId + " does not exists");
            }
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error delete barang");
            return map;
        }
    }

    @Override
    public Map getAllNative() {
        Map<String, java.io.Serializable> map = new HashMap<String, java.io.Serializable>();
        try {
            List<Object[]> obj = barangRepository.getDataAllNative();
            List<ModelBarang> dtoList = new ArrayList<>();
            for (Object[] s_detail : obj) {
                System.out.println("id = " + s_detail[0] + " nama = " + s_detail[1]);
                ModelBarang dto = new ModelBarang(Long.parseLong(s_detail[0].toString()), s_detail[1].toString());
                dtoList.add(dto);
            }
            map.put("data", (Serializable) dtoList);
            map.put("status", 200);
            map.put("message", "Get success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get barang");
            return map;
        }
    }
}
