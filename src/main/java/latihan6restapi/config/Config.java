package latihan6restapi.config;

import latihan6restapi.entity.*;
import latihan6restapi.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

@Configuration
public class Config {

    @Bean
    CommandLineRunner commandLineRunner(
            KaryawanRepository karyawanRepository,
            RekeningRepository rekeningRepository,
            DetailKaryawanRepository detailKaryawanRepository,
            TrainingRepository trainingRepository,
            KaryawanTrainingRepository karyawanTrainingRepository
    ) {
        return args -> {
            Karyawan wawan = new Karyawan(
                    1L,
                    "Wawan",
                    "Laki-laki",
                    LocalDate.of(1995, Month.MARCH, 19),
                    "Jl. Kayu Jati",
                    "2"

            );
            Karyawan yanto = new Karyawan(
                    2L,
                    "Yanto",
                    "Laki-laki",
                    LocalDate.of(1993, Month.APRIL, 10),
                    "Jl. Kayu Ulin",
                    "1"
            );
            Karyawan adi = new Karyawan(
                    3L,
                    "Adi",
                    "Laki-laki",
                    LocalDate.of(1997, Month.AUGUST, 11),
                    "Jl. Kayu Putih",
                    "1"
            );
            Karyawan wati = new Karyawan(
                    4L,
                    "Wati",
                    "Perempuan",
                    LocalDate.of(1998, Month.DECEMBER, 25),
                    "Jl. Kayu Putih",
                    "3"
            );
            karyawanRepository.saveAll(
                    Arrays.asList(wawan, yanto, adi, wati)
            );


            Rekening rekeningWawan = new Rekening(
                    1L,
                    "Rekening Wawan",
                    "Debit",
                    "783265343476",
                    wawan
            );
            Rekening rekeningYanto = new Rekening(
                    2L,
                    "Rekening Yanto",
                    "Debit",
                    "653465982365",
                    yanto
            );
            Rekening rekeningWati = new Rekening(
                    3L,
                    "Rekening Wati",
                    "Kredit",
                    "984507126534",
                    wati
            );
            rekeningRepository.saveAll(
                    Arrays.asList(rekeningWawan, rekeningYanto, rekeningWati)
            );


            DetailKaryawan detailWawan = new DetailKaryawan(
                    1L,
                    "1243920591059285",
                    "21863916",
                    wawan
            );
            DetailKaryawan detailYanto = new DetailKaryawan(
                    2L,
                    "0194812052011102",
                    "21863916",
                    yanto
            );
            DetailKaryawan detailAdi = new DetailKaryawan(
                    3L,
                    "0859104758305720",
                    "21863916",
                    adi
            );
            DetailKaryawan detailWati = new DetailKaryawan(
                    4L,
                    "9401749271057302",
                    "21863916",
                    wati
            );
            detailKaryawanRepository.saveAll(
                    Arrays.asList(detailWawan, detailYanto, detailAdi, detailWati)
            );


            Training cahyaTraining = new Training(
                    1L,
                    "Pengantar",
                    "Cahya"
            );
            trainingRepository.save(
                    cahyaTraining
            );


            KaryawanTraining karyawanTraining1 = new KaryawanTraining(
                    1L,
                    wawan,
                    cahyaTraining,
                    LocalDate.of(2021, Month.SEPTEMBER, 22)
            );
            karyawanTrainingRepository.save(
                    karyawanTraining1
            );
        };
    }
}
