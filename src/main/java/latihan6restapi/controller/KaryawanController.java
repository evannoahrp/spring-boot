package latihan6restapi.controller;

import latihan6restapi.model.UploadFileResponse;
import latihan6restapi.entity.Karyawan;
import latihan6restapi.repository.KaryawanRepository;
import latihan6restapi.service.FileStorageService;
import latihan6restapi.service.KaryawanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "v1/karyawan")
public class KaryawanController {

    private static final Logger logger = LoggerFactory.getLogger(
            KaryawanController.class
    );

    private final KaryawanService karyawanService;

    private final FileStorageService fileStorageService;

    private final KaryawanRepository karyawanRepository;

    @Autowired
    public KaryawanController(KaryawanService karyawanService,
                              KaryawanRepository karyawanRepository,
                              FileStorageService fileStorageService) {
        this.karyawanService = karyawanService;
        this.karyawanRepository = karyawanRepository;
        this.fileStorageService = fileStorageService;
    }

    @GetMapping
    public Map getAllKaryawan() {
        return karyawanService.getAll();
    }

    @GetMapping(path = "{karyawanId}")
    public Map getOneKaryawan(@PathVariable() Long karyawanId) {
        return karyawanService.getOne(karyawanId);
    }

    @PostMapping
    public Map postKaryawan(@Valid @RequestBody Karyawan karyawan) {
        return karyawanService.insert(karyawan);
    }

    @PutMapping
    public Map updateKaryawan(@Valid @RequestBody Karyawan karyawan) {
        return karyawanService.update(karyawan);
    }

    @DeleteMapping("{karyawanId}")
    public ResponseEntity<Map> softDelete(@PathVariable() Long karyawanId) {
        Map<String, Object> map = new HashMap<>();
        Karyawan obj = karyawanRepository.getbyID(karyawanId);
        obj.setDeleted_date(new Date());
        karyawanRepository.save(obj);
        map.put("data", "Delete success");
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<Map> pageList(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String namashort,
            @RequestParam() String typeShort) {
        Map<String, Object> map = new HashMap<>();
        String getshorting = namashort.equals("") ? "id" : namashort;
        Pageable show_data;
        if (typeShort.equals("desc")) {
            show_data = PageRequest.of(
                    page,
                    size,
                    Sort.by(getshorting).descending());
        } else {
            show_data = PageRequest.of(page,
                    size,
                    Sort.by(getshorting).ascending());
        }
        map.put("data", karyawanRepository.findAll(show_data));
        return new ResponseEntity<>(
                map,
                new HttpHeaders(),
                HttpStatus.OK);
    }

    @GetMapping("/listbystatus")
    public ResponseEntity<Page<Karyawan>> listByStatus(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String status) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Karyawan> list = karyawanRepository.findByStatus(status, pageable);
        return new ResponseEntity<Page<Karyawan>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/listbynama")
    public ResponseEntity<Page<Karyawan>> listByNama(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String nama) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Karyawan> list = karyawanRepository.findByNama(nama, pageable);
        return new ResponseEntity<Page<Karyawan>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(
            value = "/upload",
            method = RequestMethod.POST,
            consumes = {"multipart/form-data", "application/json"}
    )
    public UploadFileResponse uploadFile(@RequestParam() MultipartFile file,
                                         @RequestParam() Long id) throws IOException {
        try {
            fileStorageService.storeFile(file);
            Karyawan obj = karyawanRepository.getbyID(id);
            obj.setFilenama(file.getOriginalFilename());
            karyawanRepository.save(obj);
        } catch (Exception e) {
            e.printStackTrace();
            return new UploadFileResponse(
                    file.getOriginalFilename(),
                    null,
                    file.getContentType(),
                    file.getSize(),
                    e.getMessage()
            );
        }

        String fileDownloadUri = ServletUriComponentsBuilder.
                fromCurrentContextPath().
                path("/v1/karyawan/showfile/").
                path(Objects.requireNonNull(file.getOriginalFilename())).
                toUriString();

        return new UploadFileResponse(
                file.getOriginalFilename(),
                fileDownloadUri,
                file.getContentType(),
                file.getSize(),
                "false"
        );
    }

    @GetMapping("/showfile/{fileName:.+}")
    public ResponseEntity<Resource> showFile(@PathVariable String fileName,
                                             HttpServletRequest request) {
        String contentType = null;

        try {
            contentType = request.getServletContext().getMimeType(
                    fileStorageService.
                            loadFileAsResource(fileName).
                            getFile().
                            getAbsolutePath()
            );
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\""
                                + fileStorageService.loadFileAsResource(fileName).getFilename()
                                + "\""
                )
                .body(fileStorageService.loadFileAsResource(fileName));
    }
}
