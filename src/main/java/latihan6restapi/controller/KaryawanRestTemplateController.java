package latihan6restapi.controller;

import latihan6restapi.entity.Karyawan;
import latihan6restapi.service.KaryawanRestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/v1/resttemplate")
public class KaryawanRestTemplateController {

    private final KaryawanRestTemplateService karyawanRestTemplateService;

    @Autowired
    public KaryawanRestTemplateController(
            KaryawanRestTemplateService karyawanRestTemplateService) {
        this.karyawanRestTemplateService = karyawanRestTemplateService;
    }

    @GetMapping
    public Map getAllKaryawan() {
        return karyawanRestTemplateService.getAll();
    }

    @PostMapping
    public Map postKaryawan(@Valid @RequestBody Karyawan karyawan) {
        return karyawanRestTemplateService.insert(karyawan);
    }

    @PutMapping
    public Map updateKaryawan(@Valid @RequestBody Karyawan karyawan) {
        return karyawanRestTemplateService.update(karyawan);
    }

    @DeleteMapping("{karyawanId}")
    public Map softDelete(@PathVariable() Long karyawanId) {
        return karyawanRestTemplateService.delete(karyawanId);
    }
}
