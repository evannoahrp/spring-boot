package latihan6restapi.controller;

import latihan6restapi.entity.KaryawanTraining;
import latihan6restapi.repository.KaryawanTrainingRepository;
import latihan6restapi.service.KaryawanTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "v1/karyawantraining")
public class KaryawanTrainingController {

    private final KaryawanTrainingService karyawanTrainingService;

    private final KaryawanTrainingRepository karyawanTrainingRepository;

    @Autowired
    public KaryawanTrainingController(KaryawanTrainingService karyawanTrainingService,
                                      KaryawanTrainingRepository karyawanTrainingRepository) {
        this.karyawanTrainingService = karyawanTrainingService;
        this.karyawanTrainingRepository = karyawanTrainingRepository;
    }

    @GetMapping
    public Map getAllKaryawanTraining() {
        return karyawanTrainingService.getAll();
    }

    @PostMapping
    public Map postKaryawanTraining(@Valid @RequestBody KaryawanTraining karyawanTraining) {
        return karyawanTrainingService.insert(karyawanTraining);
    }

    @PutMapping
    public Map updateKaryawanTraining(@Valid @RequestBody KaryawanTraining karyawanTraining) {
        return karyawanTrainingService.update(karyawanTraining);
    }

    @DeleteMapping("{karyawanTrainingId}")
    public ResponseEntity<Map> softDelete(@PathVariable() Long karyawanTrainingId) {
        Map<String, Object> map = new HashMap<>();
        KaryawanTraining obj = karyawanTrainingRepository.getbyID(karyawanTrainingId);
        obj.setDeleted_date(new Date());
        karyawanTrainingRepository.save(obj);
        map.put("data", "Delete success");
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }
}
