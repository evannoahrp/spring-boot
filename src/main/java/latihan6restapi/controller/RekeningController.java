package latihan6restapi.controller;

import latihan6restapi.entity.Rekening;
import latihan6restapi.repository.RekeningRepository;
import latihan6restapi.service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "v1/rekening")
public class RekeningController {

    private final RekeningService rekeningService;

    private final RekeningRepository rekeningRepository;

    @Autowired
    public RekeningController(RekeningService rekeningService,
                              RekeningRepository rekeningRepository) {
        this.rekeningService = rekeningService;
        this.rekeningRepository = rekeningRepository;
    }

    @GetMapping
    public Map getAllRekening() {
        return rekeningService.getAll();
    }

    @GetMapping(path = "{rekeningId}")
    public Map getOneRekening(@PathVariable() Long rekeningId) {
        return rekeningService.getOne(rekeningId);
    }

    @PostMapping("{karyawanId}")
    public Map postRekening(@PathVariable() Long karyawanId,
                            @Valid @RequestBody Rekening rekening) {
        return rekeningService.insert(rekening, karyawanId);
    }

    @PutMapping
    public Map updateRekening(@Valid @RequestBody Rekening rekening) {
        return rekeningService.update(rekening);
    }

    @DeleteMapping("{rekeningId}")
    public ResponseEntity<Map> softDelete(@PathVariable() Long rekeningId) {
        Map<String, Object> map = new HashMap<>();
        Rekening obj = rekeningRepository.getbyID(rekeningId);
        obj.setDeleted_date(new Date());
        rekeningRepository.save(obj);
        map.put("data", "Delete success");
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<Map> pageList(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String namashort,
            @RequestParam() String typeShort) {
        Map<String, Object> map = new HashMap<>();
        String getshorting = namashort.equals("") ? "id" : namashort;
        Pageable show_data;
        if (typeShort.equals("desc")) {
            show_data = PageRequest.of(
                    page,
                    size,
                    Sort.by(getshorting).descending());
        } else {
            show_data = PageRequest.of(page,
                    size,
                    Sort.by(getshorting).ascending());
        }
        map.put("data", rekeningRepository.findAll(show_data));
        return new ResponseEntity<>(
                map,
                new HttpHeaders(),
                HttpStatus.OK);
    }
}
