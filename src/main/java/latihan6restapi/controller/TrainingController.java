package latihan6restapi.controller;

import latihan6restapi.entity.Training;
import latihan6restapi.repository.TrainingRepository;
import latihan6restapi.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "v1/training")
public class TrainingController {

    private final TrainingService trainingService;

    private final TrainingRepository trainingRepository;

    @Autowired
    public TrainingController(TrainingService trainingService,
                              TrainingRepository trainingRepository) {
        this.trainingService = trainingService;
        this.trainingRepository = trainingRepository;
    }

    @GetMapping
    public Map getAllTraining() {
        return trainingService.getAll();
    }

    @PostMapping
    public Map postTraining(@Valid @RequestBody Training training) {
        return trainingService.insert(training);
    }

    @PutMapping
    public Map updateTraining(@Valid @RequestBody Training training) {
        return trainingService.update(training);
    }

    @DeleteMapping("{trainingId}")
    public ResponseEntity<Map> softDelete(@PathVariable() Long trainingId) {
        Map<String, Object> map = new HashMap<>();
        Training obj = trainingRepository.getbyID(trainingId);
        obj.setDeleted_date(new Date());
        trainingRepository.save(obj);
        map.put("data", "Delete success");
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }
}
