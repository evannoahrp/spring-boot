package latihan6restapi.controller.tymeleaf;

import latihan6restapi.config.ResourceNotFoundException;
import latihan6restapi.entity.Karyawan;
import latihan6restapi.repository.KaryawanRepository;
import latihan6restapi.service.KaryawanTymeleafService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/v1/view/karyawan")
public class KaryawanTymeleafController {

    private final KaryawanRepository karyawanRepository;

    private final KaryawanTymeleafService karyawanTymeleafService;

    @Autowired
    public KaryawanTymeleafController(KaryawanRepository karyawanRepository,
                                      KaryawanTymeleafService karyawanTymeleafService) {
        this.karyawanRepository = karyawanRepository;
        this.karyawanTymeleafService = karyawanTymeleafService;
    }

    @GetMapping(value = {"", "/index"})
    public String index(Model model) {
        model.addAttribute("title", "Title Saya");
        return "index";
    }

    @GetMapping(value = "/list")
    public String getKaryawan(Model model,
                              @RequestParam(value = "page", defaultValue = "1") int pageNumber) {
        int ROW_PER_PAGE = 4;
        List<Karyawan> karyawan = karyawanTymeleafService.listKaryawanTymeleaf(pageNumber, ROW_PER_PAGE);
        long count = karyawanRepository.count();
        boolean hasPrev = pageNumber > 1;
        boolean hasNext = ((long) pageNumber * ROW_PER_PAGE) < count;
        model.addAttribute("karyawan", karyawan);
        model.addAttribute("hasPrev", hasPrev);
        model.addAttribute("prev", pageNumber - 1);
        model.addAttribute("hasNext", hasNext);
        model.addAttribute("next", pageNumber + 1);
        return "karyawan-list";
    }

    @GetMapping(value = {"/add"})
    public String showAddKaryawan(Model model) {
        Karyawan karyawan = new Karyawan();
        model.addAttribute("add", true);
        model.addAttribute("karyawan", karyawan);

        return "karyawan-ed";
    }

    @PostMapping(value = "/add")
    public String addBarang(Model model,
                            @ModelAttribute("karyawan") Karyawan karyawan) {
        try {
            System.out.println("Nilai karyawan = " + karyawan.getNama());
            Karyawan obj = karyawanTymeleafService.saveTymeleaf(karyawan);
            return "redirect:/v1/view/karyawan/" + String.valueOf(obj.getId());
        } catch (Exception ex) {
            String errorMessage = ex.getMessage();
            model.addAttribute("errorMessage", errorMessage);
            model.addAttribute("add", true);
            return "karyawan-ed";
        }
    }

    @GetMapping(value = {"/{karyawanId}/edit"})
    public String showEditKaryawan(Model model, @PathVariable long karyawanId) {
        Karyawan karyawan = null;
        try {
            karyawan = karyawanTymeleafService.findByIdTymeleaf(karyawanId);
        } catch (ResourceNotFoundException ex) {
            model.addAttribute("errorMessage", "Karyawan not found");
        }
        model.addAttribute("add", false);
        model.addAttribute("karyawan", karyawan);
        return "karyawan-ed";
    }

    @PostMapping(value = {"/{karyawanId}/edit"})
    public String updateKaryawan(Model model,
                                 @PathVariable long karyawanId,
                                 @ModelAttribute("karyawan") Karyawan karyawan) {
        try {
            karyawan.setId(karyawanId);
            karyawanTymeleafService.updateTymeleaf(karyawan);
            return "redirect:/v1/view/karyawan/" + String.valueOf(karyawan.getId());
        } catch (Exception ex) {
            String errorMessage = ex.getMessage();
            model.addAttribute("errorMessage", errorMessage);
            model.addAttribute("add", false);
            return "karyawan-ed";
        }
    }

    @GetMapping(value = "/{karyawanId}")
    public String getKaryawanById(Model model, @PathVariable long karyawanId) {
        Karyawan karyawan = null;
        try {
            karyawan = karyawanTymeleafService.findByIdTymeleaf(karyawanId);
        } catch (ResourceNotFoundException ex) {
            model.addAttribute("errorMessage", "Karyawan not found");
        }
        model.addAttribute("karyawan", karyawan);
        return "karyawan";
    }

    @GetMapping(value = {"/{karyawanId}/delete"})
    public String showDeleteKaryawanId(
            Model model, @PathVariable long karyawanId) {
        Karyawan karyawan = null;
        try {
            karyawan = karyawanTymeleafService.findByIdTymeleaf(karyawanId);
        } catch (ResourceNotFoundException ex) {
            model.addAttribute("errorMessage", "Karyawan not found");
        }
        model.addAttribute("allowDelete", true);
        model.addAttribute("karyawan", karyawan);
        return "karyawan";
    }

    @PostMapping(value = {"/{karyawanId}/delete"})
    public String deleteKaryawanById(
            Model model, @PathVariable long karyawanId) {
        try {
            karyawanTymeleafService.deleteByIdTymeleaf(karyawanId);
            return "redirect:/v1/view/karyawan/list";
        } catch (ResourceNotFoundException ex) {
            String errorMessage = ex.getMessage();
            model.addAttribute("errorMessage", errorMessage);
            return "karyawan";
        }
    }

}
