package latihan6restapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Setter;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "detailkaryawan")
public class DetailKaryawan implements Serializable {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "detailkaryawan_sequence"
    )
    private Long id;

    private String nik;

    private String npwp;

    @JsonIgnore
    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "karyawan_id")
    private Karyawan karyawan;

    public DetailKaryawan() {
    }

    public DetailKaryawan(long id, String nik, String npwp) {
        this.id = id;
        this.nik = nik;
        this.npwp = npwp;
    }

    public DetailKaryawan(long id, String nik, String npwp, Karyawan karyawan) {
        this.id = id;
        this.nik = nik;
        this.npwp = npwp;
        this.karyawan = karyawan;
    }
}

