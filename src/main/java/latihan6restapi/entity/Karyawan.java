package latihan6restapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Setter;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Set;

@Setter
@Entity
@Table(name = "karyawan")
@Where(clause = "deleted_date is null")
public class Karyawan extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "karyawan_sequence"
    )
    private Long id;

    private String nama;

    private String jeniskelamin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dob;

    private String alamat;

    private String status;

    private String filenama;

    @Transient
    private Integer age;

    @OneToOne(mappedBy = "karyawan")
    private DetailKaryawan detailkaryawan;

    @OneToMany(mappedBy = "karyawan", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Rekening> rekening;

    @JsonIgnore
    @OneToMany(mappedBy = "karyawan")
    Set<KaryawanTraining> karyawantraining;

    public Karyawan() {
    }

    public Karyawan(Long id, String nama, String jeniskelamin, LocalDate dob, String alamat, String status) {
        this.id = id;
        this.nama = nama;
        this.jeniskelamin = jeniskelamin;
        this.dob = dob;
        this.alamat = alamat;
        this.status = status;
    }

    public Karyawan(Long id,
                    String nama,
                    String jeniskelamin,
                    LocalDate dob,
                    String alamat,
                    String status,
                    DetailKaryawan detailkaryawan) {
        this.id = id;
        this.nama = nama;
        this.jeniskelamin = jeniskelamin;
        this.dob = dob;
        this.alamat = alamat;
        this.status = status;
        this.detailkaryawan = detailkaryawan;
    }

    public Long getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getJeniskelamin() {
        return jeniskelamin;
    }

    public LocalDate getDob() {
        return dob;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getStatus() {
        return status;
    }

    public String getFilenama() {
        return filenama;
    }

    public Integer getAge() {
        return Period.between(this.dob, LocalDate.now()).getYears();
    }

    public DetailKaryawan getDetailkaryawan() {
        return detailkaryawan;
    }

    public List<Rekening> getRekening() {
        return rekening;
    }
}
