package latihan6restapi.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;


@Setter
@Getter
@Entity
@Table(name = "karyawantraining")
@Where(clause = "deleted_date is null")
public class KaryawanTraining extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "karyawantraining_sequence"
    )
    private Long id;

    @ManyToOne
    @JoinColumn(name = "karyawan_id")
    private Karyawan karyawan;

    @ManyToOne
    @JoinColumn(name = "training_id")
    private Training training;

    private LocalDate tanggaltraining;

    public KaryawanTraining() {
    }

    public KaryawanTraining(Long id, Karyawan karyawan, Training training, LocalDate tanggaltraining) {
        this.id = id;
        this.karyawan = karyawan;
        this.training = training;
        this.tanggaltraining = tanggaltraining;
    }
}
