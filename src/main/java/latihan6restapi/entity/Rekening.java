package latihan6restapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Setter;
import lombok.Getter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "rekening")
@Where(clause = "deleted_date is null")
public class Rekening extends AbstractDate implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "rekening_sequence"
    )
    private Long id;

    private String nama;

    private String jenis;

    private String nomor;

    @JsonIgnore
    @ManyToOne(targetEntity = Karyawan.class, cascade = CascadeType.ALL)
    private Karyawan karyawan;

    public Rekening() {
    }

    public Rekening(Long id, String nama, String jenis, String nomor, Karyawan karyawan) {
        this.id = id;
        this.nama = nama;
        this.jenis = jenis;
        this.nomor = nomor;
        this.karyawan = karyawan;
    }
}

