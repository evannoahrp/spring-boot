package latihan6restapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@Entity
@Table(name = "training")
@Where(clause = "deleted_date is null")
public class Training extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "training_sequence"
    )
    private Long id;

    private String tema;

    private String namapengajar;

    @JsonIgnore
    @OneToMany(mappedBy = "training")
    Set<KaryawanTraining> karyawantraining;

    public Training() {
    }

    public Training(Long id, String tema, String namapengajar) {
        this.id = id;
        this.tema = tema;
        this.namapengajar = namapengajar;
    }
}
