package latihan6restapi.repository;

import latihan6restapi.entity.DetailKaryawan;
import latihan6restapi.entity.Karyawan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DetailKaryawanRepository extends PagingAndSortingRepository<DetailKaryawan, Long> {

    @Query("select d from DetailKaryawan d")
    public List<DetailKaryawan> getList();

    @Query("select d from DetailKaryawan d WHERE d.id = :id")
    public DetailKaryawan getbyID(@Param("id") Long id);
}
