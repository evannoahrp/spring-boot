package latihan6restapi.repository;

import latihan6restapi.entity.Karyawan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KaryawanRepository extends PagingAndSortingRepository<Karyawan, Long> {

    @Query("select k from Karyawan k")
    public List<Karyawan> getList();

    @Query("select k from Karyawan k WHERE k.id = :id")
    public Karyawan getbyID(@Param("id") Long id);

    Page<Karyawan> findByNama(String nama, Pageable pageable);

    Page<Karyawan> findByStatus(String status, Pageable pageable);
}
