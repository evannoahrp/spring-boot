package latihan6restapi.repository;

import latihan6restapi.entity.KaryawanTraining;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KaryawanTrainingRepository extends PagingAndSortingRepository<KaryawanTraining, Long> {

    @Query("select k from KaryawanTraining k")
    public List<KaryawanTraining> getList();

    @Query("select k from KaryawanTraining k WHERE k.id = :id")
    public KaryawanTraining getbyID(@Param("id") Long id);
}
