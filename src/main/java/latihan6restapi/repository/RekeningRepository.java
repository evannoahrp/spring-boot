package latihan6restapi.repository;

import latihan6restapi.entity.Rekening;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RekeningRepository extends PagingAndSortingRepository<Rekening, Long> {

    @Query("select r from Rekening r")
    public List<Rekening> getList();

    @Query("select r from Rekening r WHERE r.id = :id")
    public Rekening getbyID(@Param("id") Long id);

    Page<Rekening> findByNama(String nama, Pageable pageable);
}
