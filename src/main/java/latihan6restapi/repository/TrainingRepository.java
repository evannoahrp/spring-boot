package latihan6restapi.repository;

import latihan6restapi.entity.Training;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingRepository extends PagingAndSortingRepository<Training, Long> {

    @Query("select t from Training t")
    public List<Training> getList();

    @Query("select t from Training t WHERE t.id = :id")
    public Training getbyID(@Param("id") Long id);
}
