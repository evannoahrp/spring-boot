package latihan6restapi.service;

import latihan6restapi.entity.Karyawan;

import java.util.Map;

public interface KaryawanRestTemplateService {

    public Map getAll();

    public Map getOne(Long karyawanId);

    public Map insert(Karyawan karyawan);

    public Map update(Karyawan karyawan);

    public Map delete(Long karyawanId);
}
