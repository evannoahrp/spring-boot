package latihan6restapi.service;

import latihan6restapi.entity.KaryawanTraining;

import java.util.Map;

public interface KaryawanTrainingService {

    public Map getAll();

    public Map insert(KaryawanTraining karyawanTraining);

    public Map update(KaryawanTraining karyawanTraining);
}
