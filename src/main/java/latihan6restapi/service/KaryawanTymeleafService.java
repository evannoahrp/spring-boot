package latihan6restapi.service;

import latihan6restapi.config.BadResourceException;
import latihan6restapi.config.ResourceNotFoundException;
import latihan6restapi.entity.Karyawan;

import java.util.List;

public interface KaryawanTymeleafService {

    public List<Karyawan> listKaryawanTymeleaf(int pageNumber, int ROW_PER_PAGE);

    public boolean existsByIdTymeleaf(Long karyawanId);

    public Karyawan findByIdTymeleaf(Long karyawanId) throws ResourceNotFoundException;

    public Karyawan saveTymeleaf(Karyawan karyawan) throws BadResourceException;

    public void updateTymeleaf(Karyawan karyawan) throws ResourceNotFoundException, BadResourceException;

    public void deleteByIdTymeleaf(Long karyawanId) throws ResourceNotFoundException;

}
