package latihan6restapi.service;

import latihan6restapi.entity.Rekening;

import java.util.Map;

public interface RekeningService {

    public Map getAll();

    public Map getOne(Long rekeningId);

    public Map insert(Rekening rekening, long karyawanId);

    public Map update(Rekening rekening);
}
