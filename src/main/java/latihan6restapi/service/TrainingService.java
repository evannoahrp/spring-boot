package latihan6restapi.service;

import latihan6restapi.entity.Training;

import java.util.Map;

public interface TrainingService {

    public Map getAll();

    public Map insert(Training training);

    public Map update(Training training);
}
