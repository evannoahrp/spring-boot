package latihan6restapi.service.implement;

import latihan6restapi.config.FileStorageException;
import latihan6restapi.config.FileStorageProperties;
import latihan6restapi.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageImplement implements FileStorageService {

    private final Path fileStorageLocation;

    @Autowired
    public FileStorageImplement(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(
                fileStorageProperties.getUploadDir()
        ).toAbsolutePath().normalize();

        try {
            Files.createDirectories(
                    this.fileStorageLocation
            );
        } catch (Exception ex) {
            throw new FileStorageException(
                    "Could not create the directory where the uploaded files will be stored.",
                    ex
            );
        }
    }

    @Override
    public String storeFile(MultipartFile file) {
        try {
            if (StringUtils.cleanPath(
                    file.getOriginalFilename()
            ).contains("..")) {
                throw new FileStorageException("Sorry! Filename contains d path sequence "
                        + StringUtils.cleanPath(file.getOriginalFilename()));
            }
            Path targetLocation = this.fileStorageLocation.resolve(
                    StringUtils.cleanPath(file.getOriginalFilename())
            );
            Files.copy(
                    file.getInputStream(),
                    targetLocation,
                    StandardCopyOption.REPLACE_EXISTING
            );
            return StringUtils.cleanPath(
                    file.getOriginalFilename()
            );
        } catch (IOException ex) {
            throw new FileStorageException(
                    "Could not store file "
                            + StringUtils.cleanPath(file.getOriginalFilename())
                            + ". Please try again!",
                    ex
            );
        }
    }

    @Override
    public Resource loadFileAsResource(String fileName) {
        try {
            Resource resource = new UrlResource(
                    this.fileStorageLocation.
                            resolve(fileName).
                            normalize().
                            toUri()
            );
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileStorageException(
                        "File not found " + fileName
                );
            }
        } catch (MalformedURLException ex) {
            throw new FileStorageException(
                    "File not found " + fileName,
                    ex
            );
        }
    }
}
