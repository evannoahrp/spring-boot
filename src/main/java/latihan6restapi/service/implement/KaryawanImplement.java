package latihan6restapi.service.implement;

import latihan6restapi.config.BadResourceException;
import latihan6restapi.config.ResourceNotFoundException;
import latihan6restapi.entity.DetailKaryawan;
import latihan6restapi.entity.Karyawan;
import latihan6restapi.repository.DetailKaryawanRepository;
import latihan6restapi.repository.KaryawanRepository;
import latihan6restapi.service.KaryawanService;
import latihan6restapi.service.KaryawanTymeleafService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
@Transactional
public class KaryawanImplement implements KaryawanService, KaryawanTymeleafService {

    private final DetailKaryawanRepository detailKaryawanRepository;

    private final KaryawanRepository karyawanRepository;

    @Autowired
    public KaryawanImplement(DetailKaryawanRepository detailKaryawanRepository,
                             KaryawanRepository karyawanRepository) {
        this.detailKaryawanRepository = detailKaryawanRepository;
        this.karyawanRepository = karyawanRepository;
    }

    @Override
    public Map getAll() {
        List<Karyawan> list = new ArrayList<Karyawan>();
        Map<String, Object> map = new HashMap<>();
        try {
            list = karyawanRepository.getList();
            map.put("data", list);
            map.put("status", "200");
            map.put("message", "Get all karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error get all karyawan");
            return map;
        }
    }

    @Override
    public Map getOne(Long karyawanId) {
        Map<String, Object> map = new HashMap<>();
        try {
            Karyawan obj = karyawanRepository.getbyID(karyawanId);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Get karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error get karyawan");
            return map;
        }
    }

    @Override
    public Map insert(Karyawan karyawan) {
        Map<String, java.io.Serializable> map = new HashMap<>();
        try {
            DetailKaryawan detailKaryawan = detailKaryawanRepository.save(
                    karyawan.getDetailkaryawan()
            );
            Karyawan obj = karyawanRepository.save(karyawan);
            detailKaryawan.setKaryawan(obj);
            detailKaryawanRepository.save(detailKaryawan);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Insert karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error insert karyawan");
            return map;
        }
    }

    @Override
    public Map update(Karyawan karyawan) {
        Map<String, java.io.Serializable> map = new HashMap<String, java.io.Serializable>();
        try {
            Karyawan obj = karyawanRepository.getbyID(karyawan.getId());
            if (karyawan.getNama() != null &&
                    karyawan.getNama().length() > 0 &&
                    !Objects.equals(obj.getNama(), karyawan.getNama())) {
                obj.setNama(karyawan.getNama());
            }
            obj.setJeniskelamin(karyawan.getJeniskelamin());
            obj.setDob(karyawan.getDob());
            obj.setAlamat(karyawan.getAlamat());
            obj.setStatus(karyawan.getStatus());
            karyawanRepository.save(obj);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Update karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error update karyawan");
            return map;
        }
    }

    @Override
    public List<Karyawan> listKaryawanTymeleaf(
            int pageNumber,
            int ROW_PER_PAGE
    ) {
        List<Karyawan> karyawan = new ArrayList<>();
        Pageable sortedByIdAsc = PageRequest.of(pageNumber - 1, ROW_PER_PAGE,
                Sort.by("id").ascending());
        karyawanRepository.findAll(sortedByIdAsc).forEach(karyawan::add);
        return karyawan;
    }

    @Override
    public boolean existsByIdTymeleaf(Long karyawanId) {
        return karyawanRepository.existsById(karyawanId);
    }

    @Override
    public Karyawan findByIdTymeleaf(
            Long karyawanId
    ) throws ResourceNotFoundException {
        Karyawan karyawan = karyawanRepository.findById(karyawanId).orElse(null);
        if (karyawan == null) {
            throw new ResourceNotFoundException(
                    "Cannot find karyawan with id: " + karyawanId
            );
        } else return karyawan;
    }

    @Override
    public Karyawan saveTymeleaf(
            Karyawan karyawan
    ) throws BadResourceException {
        if (!StringUtils.isEmpty(karyawan.getNama())) {
            DetailKaryawan detailKaryawan = detailKaryawanRepository.save(
                    karyawan.getDetailkaryawan()
            );
            Karyawan temp = karyawanRepository.save(karyawan);
            detailKaryawan.setKaryawan(temp);
            detailKaryawanRepository.save(detailKaryawan);
            return temp;
        } else {
            BadResourceException exc = new BadResourceException(
                    "Failed to save karyawan"
            );
            exc.addErrorMessage("Karyawan is null or empty");
            throw exc;
        }
    }

    @Override
    public void updateTymeleaf(
            Karyawan karyawan
    ) throws ResourceNotFoundException, BadResourceException {
        if (!StringUtils.isEmpty(karyawan.getNama())) {
            if (!existsByIdTymeleaf(karyawan.getId())) {
                throw new ResourceNotFoundException(
                        "Cannot find karyawan with id: " + karyawan.getId()
                );
            }
            Karyawan temp1 = karyawanRepository.getbyID(karyawan.getId());
            if (karyawan.getNama() != null &&
                    karyawan.getNama().length() > 0 &&
                    !Objects.equals(temp1.getNama(), karyawan.getNama())) {
                temp1.setNama(karyawan.getNama());
            }
            if (karyawan.getJeniskelamin() != null &&
                    karyawan.getJeniskelamin().length() > 0 &&
                    !Objects.equals(temp1.getJeniskelamin(), karyawan.getJeniskelamin())) {
                temp1.setJeniskelamin(karyawan.getJeniskelamin());
            }
            if (karyawan.getDob() != null &&
                    !Objects.equals(temp1.getDob(), karyawan.getDob())) {
                temp1.setDob(karyawan.getDob());
            }
            if (karyawan.getAlamat() != null &&
                    karyawan.getAlamat().length() > 0 &&
                    !Objects.equals(temp1.getAlamat(), karyawan.getAlamat())) {
                temp1.setAlamat(karyawan.getAlamat());
            }
            if (karyawan.getStatus() != null &&
                    karyawan.getStatus().length() > 0 &&
                    !Objects.equals(temp1.getStatus(), karyawan.getStatus())) {
                temp1.setStatus(karyawan.getStatus());
            }
            DetailKaryawan temp2 = detailKaryawanRepository.getbyID(temp1.getDetailkaryawan().getId());
            if (karyawan.getDetailkaryawan().getNik() != null &&
                    karyawan.getDetailkaryawan().getNik().length() > 0 &&
                    !Objects.equals(temp2.getNik(), karyawan.getDetailkaryawan().getNik())) {
                temp2.setNik(karyawan.getDetailkaryawan().getNik());
            }
            if (karyawan.getDetailkaryawan().getNpwp() != null &&
                    karyawan.getDetailkaryawan().getNpwp().length() > 0 &&
                    !Objects.equals(temp2.getNpwp(), karyawan.getDetailkaryawan().getNpwp())) {
                temp2.setNpwp(karyawan.getDetailkaryawan().getNpwp());
            }
            karyawanRepository.save(temp1);
            detailKaryawanRepository.save(temp2);
        } else {
            BadResourceException exc = new BadResourceException(
                    "Failed to save karyawan"
            );
            exc.addErrorMessage("Karyawan is null or empty");
            throw exc;
        }
    }

    @Override
    public void deleteByIdTymeleaf(
            Long karyawanId
    ) throws ResourceNotFoundException {
        if (!existsByIdTymeleaf(karyawanId)) {
            throw new ResourceNotFoundException(
                    "Cannot find karyawan with id: " + karyawanId
            );
        } else {
            Karyawan obj = karyawanRepository.getbyID(karyawanId);
            obj.setDeleted_date(new Date());
            karyawanRepository.save(obj);
        }
    }
}
