package latihan6restapi.service.implement;

import latihan6restapi.entity.Karyawan;
import latihan6restapi.service.KaryawanRestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class KaryawanRestTemplateImplement implements KaryawanRestTemplateService {

    private final RestTemplateBuilder restTemplateBuilder;

    @Autowired
    public KaryawanRestTemplateImplement(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplateBuilder = restTemplateBuilder;
    }

    @Override
    public Map getAll() {
        Map<String, java.io.Serializable> map = new HashMap<>();
        try {
            String url = "http://localhost:4000/v1/karyawan";
            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Map>() {
                    }
            );
            map.put("data", (Serializable) result.getBody());
            map.put("status", "200");
            map.put("message", "Get all karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error get all karyawan");
            return map;
        }
    }

    @Override
    public Map getOne(Long karyawanId) {
        return null;
    }

    @Override
    public Map insert(Karyawan karyawan) {
        Map<String, java.io.Serializable> map = new HashMap<>();
        try {
            String url = "http://localhost:4000/v1/karyawan";
            ResponseEntity<Map> result = restTemplateBuilder.build().postForEntity(
                    url,
                    karyawan,
                    Map.class
            );
            map.put("data", (Serializable) result.getBody());
            map.put("status", "200");
            map.put("message", "Insert karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error insert karyawan");
            return map;
        }
    }

    @Override
    public Map update(Karyawan karyawan) {
        Map<String, java.io.Serializable> map = new HashMap<>();
        try {
            String url = "http://localhost:4000/v1/karyawan";
            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(
                    url,
                    HttpMethod.PUT,
                    new HttpEntity<>(karyawan),
                    new ParameterizedTypeReference<Map>() {
                    }
            );
            map.put("data", (Serializable) result.getBody());
            map.put("status", "200");
            map.put("message", "Update karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error update karyawan");
            return map;
        }
    }

    @Override
    public Map delete(Long karyawanId) {
        Map<String, java.io.Serializable> map = new HashMap<>();
        try {
            String url = "http://localhost:4000/v1/karyawan/" + karyawanId;
            restTemplateBuilder.build().delete(url);
            map.put("status", "200");
            map.put("message", "Delete karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error delete karyawan");
            return map;
        }
    }
}