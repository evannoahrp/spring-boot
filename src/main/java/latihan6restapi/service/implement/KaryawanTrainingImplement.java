package latihan6restapi.service.implement;

import latihan6restapi.entity.Karyawan;
import latihan6restapi.entity.KaryawanTraining;
import latihan6restapi.entity.Training;
import latihan6restapi.repository.KaryawanRepository;
import latihan6restapi.repository.KaryawanTrainingRepository;
import latihan6restapi.repository.TrainingRepository;
import latihan6restapi.service.KaryawanTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class KaryawanTrainingImplement implements KaryawanTrainingService {

    private final KaryawanTrainingRepository karyawanTrainingRepository;

    private final KaryawanRepository karyawanRepository;

    private final TrainingRepository trainingRepository;

    @Autowired
    public KaryawanTrainingImplement(KaryawanTrainingRepository karyawanTrainingRepository,
                                     KaryawanRepository karyawanRepository,
                                     TrainingRepository trainingRepository) {
        this.karyawanTrainingRepository = karyawanTrainingRepository;
        this.karyawanRepository = karyawanRepository;
        this.trainingRepository = trainingRepository;
    }

    @Override
    public Map getAll() {
        List<KaryawanTraining> list = new ArrayList<KaryawanTraining>();
        Map<String, Object> map = new HashMap<>();
        try {
            list = karyawanTrainingRepository.getList();
            map.put("data", list);
            map.put("status", "200");
            map.put("message", "Get all karyawan training success");
            return map;//success
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error get all karyawan training");
            return map;
        }
    }

    @Override
    public Map insert(KaryawanTraining karyawanTraining) {
        Map<String, Object> map = new HashMap<>();
        try {
            Karyawan karyawan = karyawanRepository.getbyID(karyawanTraining.getKaryawan().getId());
            Training training = trainingRepository.getbyID(karyawanTraining.getTraining().getId());
            karyawanTraining.setKaryawan(karyawan);
            karyawanTraining.setTraining(training);
            KaryawanTraining obj = karyawanTrainingRepository.save(karyawanTraining);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    @Override
    public Map update(KaryawanTraining karyawanTraining) {
        Map<String, Object> map = new HashMap<>();
        try {
            KaryawanTraining obj = karyawanTrainingRepository.getbyID(karyawanTraining.getId());
            Karyawan karyawan = karyawanRepository.getbyID(karyawanTraining.getKaryawan().getId());
            obj.setKaryawan(karyawan);
            Training training = trainingRepository.getbyID(karyawanTraining.getTraining().getId());
            obj.setTraining(training);
            obj.setTanggaltraining(karyawanTraining.getTanggaltraining());
            karyawanTrainingRepository.save(obj);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }
}
