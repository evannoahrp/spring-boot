package latihan6restapi.service.implement;

import latihan6restapi.entity.Karyawan;
import latihan6restapi.entity.Rekening;
import latihan6restapi.repository.KaryawanRepository;
import latihan6restapi.repository.RekeningRepository;
import latihan6restapi.service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RekeningImplement implements RekeningService {

    private final RekeningRepository rekeningRepository;

    private final KaryawanRepository karyawanRepository;

    @Autowired
    public RekeningImplement(RekeningRepository rekeningRepository,
                             KaryawanRepository karyawanRepository) {
        this.rekeningRepository = rekeningRepository;
        this.karyawanRepository = karyawanRepository;
    }

    @Override
    public Map getAll() {
        List<Rekening> list = new ArrayList<Rekening>();
        Map<String, Object> map = new HashMap<>();
        try {
            list = rekeningRepository.getList();
            map.put("data", list);
            map.put("status", "200");
            map.put("message", "Get all rekening success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error get all rekening");
            return map;
        }
    }

    @Override
    public Map getOne(Long rekeningId) {
        Map<String, Object> map = new HashMap<>();
        try {
            Rekening obj = rekeningRepository.getbyID(rekeningId);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Get rekening success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error get rekening");
            return map;
        }
    }

    @Override
    public Map insert(Rekening rekening, long karyawanId) {
        Map<String, Object> map = new HashMap<>();
        try {
            Karyawan karyawan = karyawanRepository.getbyID(karyawanId);
            rekening.setKaryawan(karyawan);
            Rekening obj = rekeningRepository.save(rekening);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    @Override
    public Map update(Rekening rekening) {
        Map<String, Object> map = new HashMap<>();
        try {
            Rekening obj = rekeningRepository.getbyID(rekening.getId());
            if (obj == null) {
                map.put("status", "404");
                map.put("message", "Id not found");
                return map;
            }
            obj.setNama(rekening.getNama());
            obj.setJenis(rekening.getJenis());
            obj.setNomor(rekening.getNomor());
            rekeningRepository.save(obj);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }
}
