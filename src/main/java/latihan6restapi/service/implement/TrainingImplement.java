package latihan6restapi.service.implement;

import latihan6restapi.entity.Training;
import latihan6restapi.repository.TrainingRepository;
import latihan6restapi.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class TrainingImplement implements TrainingService {

    private final TrainingRepository trainingRepository;

    @Autowired
    public TrainingImplement(TrainingRepository trainingRepository) {
        this.trainingRepository = trainingRepository;
    }

    @Override
    public Map getAll() {
        List<Training> list = new ArrayList<Training>();
        Map<String, Object> map = new HashMap<>();
        try {
            list = trainingRepository.getList();
            map.put("data", list);
            map.put("status", "200");
            map.put("message", "Get all training success");
            return map;//success
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error get all training");
            return map;
        }
    }

    @Override
    public Map insert(Training training) {
        Map<String, Object> map = new HashMap<>();
        try {
            Training obj = trainingRepository.save(training);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }

    @Override
    public Map update(Training training) {
        Map<String, java.io.Serializable> map = new HashMap<String, java.io.Serializable>();
        try {
            Training obj = trainingRepository.getbyID(training.getId());
            if (training.getTema() != null &&
                    training.getTema().length() > 0 &&
                    !Objects.equals(obj.getTema(), training.getTema())) {
                obj.setTema(training.getTema());
            }
            if (training.getNamapengajar() != null &&
                    training.getNamapengajar().length() > 0 &&
                    !Objects.equals(obj.getNamapengajar(), training.getNamapengajar())) {
            obj.setNamapengajar(training.getNamapengajar());
            }
            trainingRepository.save(obj);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Update training success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error update training");
            return map;
        }
    }
}
