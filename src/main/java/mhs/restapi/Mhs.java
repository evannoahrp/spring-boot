package mhs.restapi;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Mhs {
    String nama;
    String nim;
}
