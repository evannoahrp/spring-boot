package mhs.restapi;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/v1/mhs/")
public class MhsController {
    private static Map<String, Mhs> mhsRepo = new HashMap<>();

    static {
        Mhs mhs1 = new Mhs();
        mhs1.setNama("Budi");
        mhs1.setNim("1");
        mhsRepo.put(mhs1.getNim(), mhs1);

        Mhs mhs2 = new Mhs();
        mhs2.setNama("Wawan");
        mhs2.setNim("2");
        mhsRepo.put(mhs2.getNim(), mhs2);
    }

    @RequestMapping(value = "/")
    public ResponseEntity<Object> getMhs() {
        return new ResponseEntity<>(mhsRepo.values(), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Object> createMhs(@RequestBody Mhs mhs) {
        mhsRepo.put(mhs.getNim(), mhs);
        return new ResponseEntity<>("Mhs is created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateMhs(@PathVariable("id") String nim, @RequestBody Mhs mhs) {
        mhsRepo.remove(nim);
        mhs.setNim(nim);
        mhsRepo.put(mhs.getNim(), mhs);
        return new ResponseEntity<>("Mhs is updated successfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteMhs(@PathVariable("id") String id) {
        mhsRepo.remove(id);
        return new ResponseEntity<>("Mhs is deleted successfully", HttpStatus.OK);
    }
}
