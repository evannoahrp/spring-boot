package student.config;

import student.entity.Student;
import student.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

@Configuration
public class StudentConfig {
    @Bean
    CommandLineRunner commandLineRunner(
            StudentRepository repository) {
        return args -> {
            Student evan = new Student(
                    1L,
                    "Evan",
                    "evan@gmail.com",
                    LocalDate.of(2012, Month.MARCH, 19)
            );
            Student budi = new Student(
                    2L,
                    "Budi",
                    "budi@gmail.com",
                    LocalDate.of(2010, Month.APRIL, 10)
            );
            repository.saveAll(
                    Arrays.asList(evan, budi)
            );
        };
    }
}
