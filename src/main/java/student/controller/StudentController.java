package student.controller;

import student.entity.Student;
import student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "v1/student")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public Map getStudents() {
        return studentService.getStudents();
    }

    @PostMapping
    public Map postStudent(@RequestBody Student student) {
        return studentService.newStudent(student);
    }

    @DeleteMapping(path = "{studentId}")
    public Map deleteStudent(@PathVariable("studentId") Long studentId) {
        return studentService.deleteStudent(studentId);
    }

    @PutMapping(path = "{studentId}")
    public void updateStudent(
            @PathVariable("studentId") Long studentId,
            @RequestBody(required = false) String name,
            @RequestBody(required = false) String email) {
        studentService.updateStudent(studentId, name, email);
    }
}
