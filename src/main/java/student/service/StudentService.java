package student.service;

import student.entity.Student;

import java.util.Map;
public interface StudentService {

    public Map getStudents();

    public Map newStudent(Student student);

    public Map deleteStudent(Long studentId);

    public void updateStudent(Long studentId, String name, String email);
}
