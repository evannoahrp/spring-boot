package student.service.implement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import student.entity.Student;
import student.repository.StudentRepository;
import student.service.StudentService;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class StudentImplement implements StudentService {

    @Autowired
    public StudentRepository studentRepository;


    public Map getStudents() {
        List<Student> l = new ArrayList<Student>();
        Map m = new HashMap();
        try {
            l = studentRepository.findAll();
            m.put("data", l);
            m.put("status", "200");
            m.put("message", "Success");
            return m;
        } catch (Exception e) {
            e.printStackTrace();
            m.put("status", "500");
            m.put("message", e);
            return m;
        }
    }

    public Map newStudent(Student student) {
        Map m = new HashMap();
        try {
            Optional<Student> sOptional = studentRepository
                    .findStudentByEmail(student.getEmail());
            if (!(sOptional.isPresent())) {
                Student obj = studentRepository.save(student);
                m.put("data", obj);
                m.put("status", "200");
                m.put("message", "Success");
            } else {
                m.put("data", "0");
                m.put("status", "500");
                m.put("error", "Internal Server Error");
                m.put("message", "Email Taken");
            }
            return m;
        } catch (Exception e) {
            e.printStackTrace();
            m.put("status", "500");
            m.put("message", e);
            return m;
        }
    }

    public Map deleteStudent(Long studentId) {
        Map m = new HashMap();
        try {
            boolean temp = studentRepository.existsById(studentId);
            if (temp) {
                studentRepository.deleteById(studentId);
                m.put("data", "1");
                m.put("status", "200");
                m.put("message", "Success");
            } else {
                m.put("data", "0");
                m.put("status", "500");
                m.put("error", "Internal Server Error");
                m.put("message", "Student with id " + studentId + " does not exists");
            }
            return m;
        } catch (Exception e) {
            e.printStackTrace();
            m.put("status", "500");
            m.put("message", e);
            return m;
        }
    }

    public void updateStudent(Long studentId, String name, String email) {
        Student student = studentRepository.getbyID(studentId);
//                .orElseThrow(() -> new IllegalStateException(
//                        "Student with id " + studentId + " does not exists"
//                ));
        if (name != null &&
                name.length() > 0 &&
                !Objects.equals(student.getName(), name)) {
            student.setName(name);
        }
        if (email != null &&
                email.length() > 0 &&
                !Objects.equals(student.getEmail(), email)) {
            Optional<Student> sOptional = studentRepository
                    .findStudentByEmail(email);
            if (sOptional.isPresent()) {
                throw new IllegalStateException("Email Taken");
            }
            student.setEmail(email);
        }
    }
}
